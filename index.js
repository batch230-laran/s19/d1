// console.log("Hello World!");



// CONDITIONAL STATEMENTS



//What are conditional statements?

//Conditional statements allow us to control the flow of our program. It allows us to run a statement/instruction if a condition is met or run another separate instruction if otherwise.



// IF, ELSE IF, and ELSE STATEMENT



// ---------------------------------------



// IF (number)



// if statement

// if(condition) {
	//code to execute
// }




let numA = -1;

if (numA < 0) {
	console.log("Hello");
}



console.log(numA < 0);    // true or false outcome



//results to true and so, the if statement was run.



// re-assign numA to 0


numA = 0


if (numA < 0) {
	console.log('Hello again if numA is lesser than zero.')
}


// it will not display the console because it did not satisfy the condition which is numA lesser than 0


/*

Equality
== : check if the same value

Strict Equality
=== : check if the same value and the same data type

*/




// ---------------------------------------



// IF (string)



let city = "New York";

if (city === "New York") {
	console.log("Welcome to New York City!");
}



// Example checking Username


let userName = "bhoxzMapagmahal";

if (userName === "bhoxzMapagmahal") {
	console.log("Welcome user " + userName);
}







// ---------------------------------------



// ELSE IF CLAUSE



/* 

- Executes a statement if previous conditions are false and if the specified condition is true

- The "else if" clause is optional and can be added to capture additional conditions to change the flow of a program

*/




let numH = 1;

if (numH < 0) {
	console.log('Hello');
}

else if (numH > 0) {
	console.log('Hi');
}



// We were able to run the else if() statement after we evaluated that the if condition was failed.


// If the if() condition was passed and run, we will no longer evaluate to else if() and end the process there.



// Another example "if" condition is satisfied


city = 'Tokyo';

if (city === "Tokyo") {
	console.log("Welcome to Tokyo, Japan");
}

else if (city === "New York") {
	console.log("Welcome to New York City");
}






// ---------------------------------------



// ELSE STATEMENT



/* 

- Executes a statement if all other conditions are false

- The "else" statement is optional and can be added to capture any other result to change the flow of a program

*/


// scroll up, numA value is 0
// scroll up, numH value is 1


if (numA > 0) {
	console.log("Hi! I'm the first option");
}

else if (numH === 0) {
	console.log("Hi! I'm the second option");
}

else if (numA === 100) {
	console.log("Hi! I'm the third option");
}

else {
	console.log ("Hi! I'm the last option");
}



// if all conditions are not satisfied (else if), (else) will display/execute

// Structure: 'If', 'else if', 'else'

// 'If' can can without 'else if' and 'else'

// but 'else if' and 'else' cannot run without 'if'






// You can also use 'if' and 'else' if you have 2 scenarios only

// example

let shelovesme = true;

if (shelovesme === true) {
	console.log ("Hi, is it me you're looking for?");
}

else {
	console.log ("Thank you, come again");
}



/*

Since both the preceding if and else if conditions failed, the else statement was run instead.

Else statements should only be added if there is a preceding if condition. else statements by itself will not work, however, if statements will work even if there is no else statement.

*/



function determineTyphoneIntensity(windSpeed) {
	if (windSpeed < 30){
		return 'Not a typhoon yet.';
	}

	else if (windSpeed <= 61) {
		return 'Tropical depression detected.';
	}

	else if (windSpeed >= 62 && windSpeed <= 88) {
		return 'Tropical storm detected.';
	}

	else if (windSpeed >= 89 || windSpeed <= 117) {
		return 'Severe tropical storm detected.';
	}

	else {
		return 'Typhoon detected.';
	}
}

let message = determineTyphoneIntensity(70);
	console.log(message);


if (message == 'Tropical storm detected.') {
	console.warn(message);
}



// && (AND) operator should satisfy both condition

// || (OR) operator only one condition must be satisfied


// you can edit the condition of windSpeed inside ():
	// let message = determineTyphoneIntensity(80);
	//    console.log(message);


// console options: console.log, console.warn, console.error








// ---------------------------------------



// TRUTHY AND FALSY



/* 

- In JavaScript a "truthy" value is a value that is considered true when encountered in a Boolean context

- Values are considered true unless defined otherwise

- Falsy values/exceptions for truthy:
  
  1. false
  2. 0
  3. -0 (there is no negative zero, we don't usually want to have a negative zero value in our output)
  4. ""
  5. null
  6. undefined
  7. NaN (Not a Number), 
     example where we will encounter NaN:  console.log('six'/12);

*/


// Truthy Examples

/* 

- If the result of an expression in a condition results to a truthy value, the condition returns true and the corresponding statements are executed

- Expressions are any unit of code that can be evaluated to a value

*/







// ---------------------------------------


// TRUTHY EXAMPLE


if (true) {
	console.log('Truthy');
}

if (1) {
	console.log('Truthy');
}



// EMPTY ARRAY TRUTHY


if ([]) {
	console.log('Truthy');
}








// ---------------------------------------


// FALSY EXAMPLES



if (false) { 
    console.log('Falsy');
}

if (0) { 
    console.log('Falsy');
}

if (undefined) { 
    console.log('Falsy');
}






// ---------------------------------------


// CONDITIONAL (TERNARY) OPERATOR



/* 

- The Conditional (Ternary) Operator takes in three operands:
   
   1. condition
   2. expression to execute if the condition is truthy
   3. expression to execute if the condition is falsy
    - Can be used as an alternative to an "if else" statement
    - Ternary operators have an implicit "return" statement meaning that without the "return" keyword, the resulting expressions can be stored in a variable
    - Commonly used for single statement execution where the result consists of only one line of code
    - For multiple lines of code/code blocks, a function may be defined then used in a ternary operator
    - Syntax
        (condition) ? ifTrue : ifFalse;

*/







// ---------------------------------------


// SINGLE STATEMENT EXECUTION (2 possible scenarios)


let ternaryResult = (18 < 18) ? true : false;
	console.log("Result of Ternary Operator: " + ternaryResult);


	// OR (different template)


/*

let ternaryResult = (18 < 18) ? "yes" : "no";
	console.log("Result of Ternary Operator: " + ternaryResult);

*/









// ---------------------------------------


// MULTIPLE STATEMENT EXECUTION (many scenarios)



function isOfLegalAge() {
	name = 'John';
	return 'You are of the legal age limit';
}

function isUnderAge() {
	name = 'Jane';
	return 'You are under the age limit';
}


// upper template using let

let age = parseInt (prompt ("What is your age?"));
	console.log(age);

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
	console.log("Result of Ternary operator in function: " + legalAge + ', ' + name);



// ------ You can use the upper template or the lower template 



// upper template using 'if' and 'else'


if (age > 18) {
	isOfLegalAge();
}

else {
	isUnderAge();
}









// ---------------------------------------


// SWITCH STATEMENT



/* 

-The switch statement evaluates an expression and matches the expression's value to a case clause. The switch will then execute the statements associated with that case, as well as statements in cases that follow the matching case.

- Can be used as an alternative to an if, "else if and else" statement where the data to be used in the condition is of an expected input

- The ".toLowerCase()" function/method will change the input received from the prompt into all lowercase letters ensuring a match with the switch case conditions if the user inputs capitalized or uppercased letters

- The "expression" is the information used to match the "value" provided in the switch cases

- Variables are commonly used as expressions to allow varying user input to be used when comparing with switch case values

- Switch cases are considered as "loops" meaning it will compare the "expression" with each of the case "values" until a match is found

- The "break" statement is used to terminate the current loop once a match has been found

- Removing the "break" statement will have the switch statement compare the expression with the values of succeeding cases even if a match was found

- Syntax

  Switch (expression) {
 	 case value:
  		statement;
  		break;

  	 default:
  		statement;
  		break;
  }

*/




let day = prompt ("What day of the week is it today?").toLowerCase();
	console.log(day);

switch (day) {
	case 'monday':
		console.log('The color of the day is red');
		break;

	case 'tuesday':
		console.log('The color of the day is orange');
		break;	

	case 'wednesday':
		console.log('The color of the day is yellow');
		break;

	case 'thursday':
		console.log('The color of the day is green');
		break;

	case 'friday':
		console.log('The color of the day is blue');
		break;

	case 'saturday':
		console.log('The color of the day is indigo');
		break;

	case 'sunday':
		console.log('The color of the day is violet');
		break;

	default:
		console.log("Please input a valid day");
		break;
}



// default acts like an 'else' statement, executes if all cases are not satisfied

// if there is no 'break;', it will run the next cases

// if first or second part is satisfied 'break;' will act as a stopper











// ---------------------------------------


// TRY-CATCH-FINALLY STATEMENT



/*

- "try catch" statements are commonly used for error handling

- There are instances when the application returns an error/warning that is not necessarily an error in the context of our code

- These errors are a result of an attempt of the programming language to help developers in creating efficient code

- They are used to specify a response whenever an exception/error is received

- It is also useful for debugging code because of the "error" object that can be "caught" when using the try catch statement

- In most programming languages, an "error" object is used to provide detailed information about an error and which also provides access to functions that can be used to handle/resolve errors to create "exceptions" within our code

- The "finally" block is used to specify a response/action that is used to handle/resolve errors

*/




function showIntensityAlert(windSpeed) {
	try {
		alerat(determineTyphoneIntensity(windSpeed));
	}

	catch (error) {
		console.warn(error.message);
	}

	finally {
		alert ('Intensity updates will show new alert');
	}

	console.log("Will still run");
}


showIntensityAlert(56);

console.log("Will still run p.2");




// 'finally' - Continue execution of code regardless of success and failure of code execution in the 'try' block to handle/resolve errors


// you can remove 'console.log("Will still run");'' and 'console.log("Will still run p.2");'' if you want



